3D Print STL Files
==================
This folder contains all STL files required for building the scanner platform. The design is based on the excellent open Ratrig V-Minion platform taken from https://github.com/Rat-Rig/V-Minion.

With permission of RatRig, some of these parts have been heavily customized to suit our needs. You can recognize these parts by the ``-tpl`` at the end of the filename.

Download
---------
You can download the entire folder as an auto-generated zip file using this link: https://gitlab.com/2pi-labs/projects/radarscanner/-/archive/main/radarscanner-main.zip?path=stl

Print Settings
--------------
Use Ratrig's recommended print settings.

| Parameter     | Value            |
| ---           | ---              |
| Perimeters    | 4x 0.45mm        |
| Layer Height  | 0.2mm or 0.3mm   |
| Infill        | 25%              |
| Supports      | None             |
| Material      | PETG or ASA/ABS  |

Note that your printer needs to be calibrated for precise prints and account for the shrinkage of the material. Popular 3D printing materials such as PLA, PETG and ABS materials have a shrinkage of roughly 99.5%. You should enter the shrinkage value in the filament section of your slicer (e.g. PrusaSlicer, SuperSlicer and variants) or scale the print by 100.5% (e.g. Cura). This is usually enough to get accurate prints, but we recommend you print a known calibration object (a Cube or a CaliFlower by Vector3D) to verify the dimensions if you are unsure.

Part Print Orientation
-----------------
The parts have been designed to have a specific print orientation. Refer to the following image from RatRig for the correct orientation for printing.

![Part Orientation](part-orientation.png "Part Orientation")

Note that some of the parts in this folder are not shown in the image, but their print orientations are mostly obvious. If you have issues printing, feel free to contact us.

