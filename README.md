Radarscanner Platform
=====================
The Radarscanner project is a positioning platform for our 2piSENSE series radar sensors. It can be used for generating spatially resolved radar images (real aperture or SAR images) with an X-Y scan area of approximately 180x180 mm². 

This project is open-source and free to build for anyone. It is based on the popular and excellent [V-Minion](https://ratrig.com/v-minion.html) project of RatRig. The required mechanical parts can be sourced yourself or bought as a kit. Some parts of the this project are 3D printed parts that you need to print yourself or have them printed by a 3rd party.

We highly recommend that you read through this entire page first, before ordering anything.

![Radarscanner at IRMMW Montreal](doc/image/radarscanner-irmmw-montreal.jpg "Radarscanner at IRMMW Montreal")


What do I need?
---------------
The best way to start is to buy a V-Minion configurable kit for all the mechanical parts from the RatRig website. See the [BOM](./BOM.md) file in this repository for more information. Make sure you follow the BOM exactly so that you order the correct parts for this project.

In addition you will need a 2piSENSE series radar sensor (e.g. [2piSENSE X1155S scientific USB radar](https://www.2pi-labs.com/en/radar-sensor-products/scientific-radar/)). We highly recommend that you use hardware-triggering for achieving the best scan speeds (see BOM). Hardware triggering uses the unused extruder axis as a trigger signal to the radar sensor and thus allowing very precise scanning without using slow stop-and-go scanning. 
For connecting the radar sensor to the extruder axis, we recommend using the stepper-adapter module in the [kicad](./kicad/) folder. This module is inserted on the mainboard *instead* of the stepper driver and re-uses the 4-pin stepper connector for the radar I/O connection.


Where do I start?
-----------------
Start with building the basic mechanical platform using RatRigs excellent build guide [here](https://ratrig.dozuki.com/c/02._Rat_Rig_V-Minion). Follow Steps 01. through 05. and leave out part *08. Electronics Enclosure* and *09. Wiring & Firmware* since we will be using another firmware here.

Note that some parts (e.g. such as the spool holder and EVA assembly) are unnecessary for the scanner. Also, you may not find a guide for some printed parts or the printed parts look different from the guide. This is okay, since we have modified/added some parts of the design to fit our needs. For now we don't have a separate guide but you can always check our CAD design files and images in the [CAD](./cad/) folder if you are stuck.


How do I wire the scanner up?
-----------------------------
First you should start with wiring your scanner up. For the basic connection of the *stepper motors*, *endstops* and *inductive probe*, follow the *Wiring Diagram* of the *BIGTREETECH SKRat 1.0* board in the *09. Wiring & Firmware* section in the official build guide [here](https://ratrig.dozuki.com/Guide/09.+Wiring+&+Firmware/131). Omit all other non-present electrical parts such as fans or heaters. Make sure to correctly configure the jumpers as shown in the pictures.

We recommend that you use our own electronics-box design instead of the electronics enclosuse by RatRig, which will give you a cleaner and more portable solution. In this case, the power supply in RatRigs documents is replaced by a Neutrik connector for an external power supply. Refer to this image for an overview of the required wiring.

![Wiring](doc/image/radarscanner-wiring.png "Wiring")

If you plan to use the hardware trigger, you need to crimp a JST XH 4-pin connector to the 4-wire M8 sensor cable as shown in this picture so it can be connected to the connector of the designated stepper axis. For the chosen stepper axis, you need to place the *stepper-adapter* board in the corresponding socket.

![Radar cable crimp](doc/image/radarscanner-m8-crimp.jpg "Radar cable crimp")
![Stepper Adapter](doc/image/radarscanner-stepper-adapter.png "Stepper Adapter")


How do I make it run?
----------------------
As a last step you need to program the SKRat mainboard with the correct firmware. Since we are not using the RatOS by RatRig, which is designed for 3D printers you need to program the firmware in this repository, which is build upon the well known Marlin firmware. This involves flashing the microcontroller with a binary file by using dfu-utils for example. See the [firmware](./firmware/) folder for more information on how to do this.

Things to check before you run it
---------------------------------

1. Did you oil/grease the linear rails according to the build guide?
1. Make sure that the inductive probe has a metallic Z-endstop (e.g. M3x8 screw) to home against
1. Check the wiring of your external power supply
1. Assure that belts are neither too tight, nor too lose
1. Is the radar securely mounted to the carriage?
1. The wiring loom needs to be able to move freely, without tight bends on the X and Z axis
1. Test the endstops by hand, a light should go on or off, when you press X/Y endstop or the Z probe detects something metallic.
1. Have your hand at the power connector to be able to quickly turn the scanner off, during the first homing move
