Bill of Materials
=================

Mechanical
----------
We recommend ordering a V-Minion configurable Kit from RatRig [here](https://ratrig.com/vminion-configurable.html). Choose the following configuration:

- 1 x V-Minion - Mechanical Kit
- 1 x BigTreeTech SKRat V1.0 Motherboard (board only - no drivers) 
- 4 x BigTreeTech TMC2209 Driver 
- 3 x Nema 17 Stepper Motor - 40mm - 1.8degree/step, 61oz-in (LDO-42STH40-1684AC) 
- 1 x FlexPlate PRO Set - Black Textured PEI 184 x 184 mm - Double Sided
- 2 x Endstop Module - Horizontal Limit switch only 
- 2 x Cable 1000mm - 3 Conductor 24AWG - JST XH2.54 to JST XH2.54 (Endstop pin out) 
- 1 x Inductive Probe 5V - M8 (5A LJ8A3-2-Z/AX) 
- 3 x Cable 1000mm - 4 Conductor 24AWG - JST XH2.54 to JST PH6P (Nema 17 connector)

Electronics Box
---------------
We recommend you using our own 3D printed electronics box that is mounted directly to the V-Minion frame and is thus a very clean and mobile solution. For this you need additionally:

- 1x Neutrik NC4FD-LX XLR connector
- 1x External 24V power supply, e.g. OWA-120E-24 (24V, 5A with matching XLR connector)

Hardware Trigger
----------------
We recommend to use the hardware trigger function of the scanner. It uses the extruder axis STEP output to trigger the radar measurement and thus allows very precisely controlled radar acquisitions, even during motion (no stop-and-go necessary) delivering fast and high quality scans. For this you will need:

- 1x 4-pin M8 cable, e.g. PHOENIX CONTACT SAC-4P-5.0-PUR/M 8FS
- 1x 4-pin JST XH connector. Probably best to get a full JST XH kit from Amazon, including crimping tool.
- 1x Stepper-Adapter Board from this project. This board can be sourced from JLCPCB by uploading the corresponding gerber/assembly files.
  As a workaround, you can also use jumper wires for breadboards from Amazon, to connect the stepper signals to the radar connector, but this is not really recommended.


