You can use the following images if you get stuck. Alternatively you can open the STEP file in a CAD software (e.g. CAD Assistant) and zoom your way around the model.

![Front](radarscanner-cad-front.png "Front")

![Back](radarscanner-cad-back.png "Back")

