Flashing Marlin Firmware
------------------------
In general there are two ways, to flash the Marlin .bin file to your SKR board. Either using dfu-util and the built-in bootloader mode or using the SD card. We recommend using the SD card to program your board.

1. Put the (Micro-) SD card of your SKR board into a Windows or Linux computer.
1. Copy the ``sarscanner-marlin-x.y.z.a.bin`` file on the SD card and rename it to ``firmware.bin``
1. Put the (Micro-) SD card back into your SKR board and power it up

